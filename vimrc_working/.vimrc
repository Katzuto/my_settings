" *************************
" *   Plugins / Плагины   *
" *************************

" Specify a directory for plugins
call plug#begin('~/.vim/plugged')

" Дерево проекта
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }

" Цветовая схема
Plug 'tomasiser/vim-code-dark'
Plug 'altercation/vim-colors-solarized'
Plug 'tomasr/molokai'

" Автодополнение
Plug 'ycm-core/YouCompleteMe'

" Автоматически создает пару к скобкам, ковычкам
Plug 'jiangmiao/auto-pairs'

" Для вызова команд git'а, с помощью :G<команда_git>
Plug 'tpope/vim-fugitive'

" Показывает изменения в файле относительно последнего коммита
Plug 'airblade/vim-gitgutter'

" Нечеткий поиск по файлам проекта, ctrl+p для вызова
Plug 'kien/ctrlp.vim'

" Нижняя панель
Plug 'vim-airline/vim-airline'

" Дерево функций
Plug 'majutsushi/tagbar'

" Поиск по файлам
Plug 'dkprice/vim-easygrep'

" CScope
Plug 'chazy/cscope_maps'

" Initialize plugin system
call plug#end()


" ****************************
" *   Settings / Настройки   *
" ****************************

" General settings - Общие настройки
" ----------------------------
set expandtab

set number		" - Включить номера строк

syntax enable	" - Включить подсветку синтаксиса

set hlsearch		" - Подсветка всех найденых совпадений
set incsearch		" - Инкрементный поиск

"set fileencoding=cp1251  " - Установить кодировку для файлов
" ---------------------------- (General settings)

" Color-Scheme - Настройки для цветовой схемы
" ----------------------------
set background=dark
colorscheme solarized
let g:solarized_termcolors=256
set guifont=Hack\ 11
" ---------------------------- (Color-Scheme)

" Airline - Настройка линии airline
" ----------------------------
let g:aviation_powerline_fonts=1

set noshowmode
if !exists('g:airline_symbols')
let g:airline_symbols = {}
endif
" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.linenr = '☰'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.notexists = '∄'
let g:airline_symbols.whitespace = 'Ξ'

" powerline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = '☰'
let g:airline_symbols.maxlinenr = ''
" -------------------------------- (Airline)

" YouCompeteMe - Настройки для YouCompeteMe
" --------------------------------
set completeopt-=preview " - Отключить окно предпросмотра в YouCompleteMe
" -------------------------------- (YouCompleteMe)

" Vim-gitgutter - Настройка для Vim-gitgutter
" --------------------------------
set updatetime=100		" - Задержка обновления для vim-gitgutter
" -------------------------------- (Vim-gitgutter)

" Настройка CodeStyle для скриптов
" -------------------------------- 
set tabstop=2       " - Установить размер таба в 2 пробела
set shiftwidth=2    " - Установить размер отступа в 2 пробела
" -------------------------------- (CodeStyle для скриптов)

" Настройка CodeStyle для исходников
" -------------------------------- 
autocmd FileType c,cpp,h,hpp
\ set tabstop=5 |
\ set shiftwidth=5
" -------------------------------- (CodeStyle для исходников)

" Настройка CodeStyle для Makefil'ов
" -------------------------------- 
au FileType make 
\ set noexpandtab |
\ set tabstop=8 |
\ set shiftwidth=8
" -------------------------------- (CodeStyle для Makefil'ов)


" ***********************************
" *   Mappings / Сочетания клавиш   *
" ***********************************

" Leader	- Главный символ ( по умолчанию '\' )
" C 	    - Ctrl
" CR      - Enter
" A       - Alt
"
" Left 	- Arrow left
" Down 	- Arrow down
" Up	     - Arrow up
" Right	- Arrow right

" Ctrl + n - Для открытия дерева проекта
map <C-n> :NERDTreeToggle<CR>

" F8 - Для открытия дерева функций
nmap <F8> :TagbarToggle<CR>

" Ctrl + [h, j, k, l] - Для перемещения по окнам
map <silent> <C-h> :call WinMove('h')<CR>
map <silent> <C-j> :call WinMove('j')<CR>
map <silent> <C-k> :call WinMove('k')<CR>
map <silent> <C-l> :call WinMove('l')<CR>

" Ctrl + [Arrows] - Для перемещения по окнам
map <silent> <C-Left> :call WinMove('h')<CR>
map <silent> <C-Down> :call WinMove('j')<CR>
map <silent> <C-Up> :call WinMove('k')<CR>
map <silent> <C-Right> :call WinMove('l')<CR>

" Ctrl + Tab - Создать новую вкладку
map <C-TAB> :tabnew<CR>
" Alt + Down - Закрыть вкладку
map <A-Down> :tabc<CR>
" Alt + [left, right] - Перемещение по открытым вкладкам
map <A-Left> :tabp<CR>
map <A-Right> :tabn<CR>

" Ctrl + s - Сохранить файл
map <C-s> :w<CR>

" Ctrl + f - Поиск выделенного элемента
map <C-f> y/<C-r>"<CR>

" F2 - Найти определение
nmap <F2> <C-]>
" F3 - Поиск использования (любого) имени под курсором
nmap <F3> :vert scs find s <C-R>=expand("<cword>")<CR><CR> 

" Ctrl + d - git diff этого открытого файла
map <C-d> :Gdiff<CR>

" ***************************
" *   Functions / Функции   *
" ***************************

" Функция для перемещения по окнам
function! WinMove( key )
	let t:curwin = winnr()
	exec "wincmd ".a:key
	if( t:curwin == winnr() )
		if( match( a:key, '[jk]' ) )
			wincmd v
		else
			wincmd s
		endif
		exec "wincmd ".a:key
	endif
endfunction


" PVS
" -------------------------------- 
" Функция для PVS анализа
function! StartAnalysePvs()
     set makeprg=cat\ %
          silent make
	       cw
endfunction

" Открываем project.err и набираем \pvs
command! StartAnalysePvs :call StartAnalysePvs()
noremap \pvs :StartAnalysePvs<cr>
" -------------------------------- (PVS)

